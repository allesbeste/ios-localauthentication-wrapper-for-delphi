# README #

This README documents how this code can be used to implement TouchID in a Delphi iOS application.

The iOS LocalAuthentication framework, which is responsible for TouchID authentication, is available at https://developer.apple.com/library/ios/documentation/LocalAuthentication/Reference/LocalAuthentication_Framework/

### How do I get set up? ###

The iOS LocalAuthentication framework should be included in the SDK manager in the Delphi IDE (it is not by default).
In the Delphi IDE, got to Tools->Options->Environment Options->SDK Manager. Choose the active iOS SDK version under "SDK Versions", eg iPhoneOS 9.3. On the right under Remote Paths, if you scroll down you will see the frameworks that have been added to your environment, eg. Contacts, UIKit, etc. LocalAuthentication should be there. If it is not, you should add it. To do this, double click one of the other frameworks, eg. AddressBook. Copy the path to the clipboard (eg, $(SDKROOT)/System/Library/Frameworks). Click Cancel. Click the "Add new path item"-button on the far right. Paste the copied path in the path input box. In the Framework Name input box, type LocalAuthentication. All radio buttons under "Path type" should be deselected. "Include non-essential framework files" should be deselected. Click OK. Make sure your Mac is running with PAServer. Click the "Update Local File Cache" button. It should now download the SDK framework files for the LocalAuthentication framework.

In Delphi, go to Project -> Options -> Delphi Compiler -> Linking -> Options passed to the LD Linker. Set it to "-framework LocalAuthentication". Make sure this is set for any iOS target you are going to compile for.

The iOSapi.LocalAuthentication.pas file should be copied to a folder included in the PATH variable. iOSapi.LocalAuthentication should then be included in your unit's uses clause.

The LAContext instance class includes two methods :

* function canEvaluatePolicy(policy: LAPolicy; error: NSError) : Boolean // Used to determine if it is possible to evaluate the defined authentication policy
* procedure evaluatePolicy(policy: LAPolicy; localizedReason: NSString; reply: LAContextReply) // Used to authenticate against the defined policy

The evaluatePolicy method uses a callback function, called reply, that is executed after authentication has completed. The callback function passes a "success" Boolean value to indicate if authentication was successful. It also passes an "error" object that contains details of any errors which might have occurred. If authentication was successful the "error" object will be nil.

An example of implementing this in your application is :


```
#!delphi

uses
    iOSapi.LocalAuthentication, Macapi.Helpers;

procedure TForm1.TouchIDReply(success: Pointer; error: Pointer);
var
    iSuccess : Integer;
    bSuccess : Boolean;
begin
    bSuccess := false;
    if not Assigned(error) then
    begin
        if Assigned(success) then
        begin
            iSuccess := Integer(success);
            bSuccess := (iSuccess = 1);
        end;
    end;
    if bSuccess then
    begin
        // Success code
    end
    else
    begin
        // Failure code
    end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
    Context : LAContext;
    canEvaluate : Boolean;
begin
    Context := TLAContext.Alloc;
    Context := TLAContext.Wrap(Context.init);
    canEvaluate := Context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, nil);
    if canEvaluate = true then
    begin
        Context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, StrToNSStr('Your localized text here'), TouchIDReply);
    end;
    if dieResult = false then
    begin
        // Cannot evaluate specific policy
    end;
    Context.release;
end;
```


### Contribution guidelines ###

Contribution is encouraged. Please feel free to fork, improve and submit pull requests.
The issue tracker is also available to submit issues and improvements.